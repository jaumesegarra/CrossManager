## TODO
- [x] Comprobar campos vacios al guardar. Poner * en el label de ellos
- [x] Comprobar hora correcta en CarreraWindow
- [x] No dejar aprobar una Carrera si su recorrido no está aporobado
- [x] Añadir voluntarios y listarlos
- [x] Informe de voluntarios
- [x] Informes ganadores carrera y clubs
- [x] Pasar metodos a clases DAO
- [x] Ventana preferencias Base de datos
- [ ] Poder crear automáticamente la base de datos si no existe
- [x] Implementar manual de usuario
- [x] Comprobar fuera plazo de inscripción del corredor
- [ ] Comprobar si la carrera está en marcha al retirar dorsal
- [ ] Mas datos en el informe de aportaciones
- [x] Informes en HTML
- [ ] Comentar código

# No da tiempo
- [ ] Filtrar tablas (http://www.java2s.com/Tutorial/Java/0240__Swing/Tablerowfilerandsortkey.htm)
- [ ] Hacer rifa con regalos no repartidos
- [ ] Refactorizar codigo