/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class AportacionObjeto extends Aportacion{
    private int cantidad;
    private int cantidad_disponible;
    private int cantidad_no_endeudada;
    
    public AportacionObjeto(int id, String concepto, Patrocinador aportador, int cantidad, int cantidad_disponible){
        super(id, concepto, aportador);
        this.cantidad=cantidad;
        this.cantidad_disponible=cantidad_disponible;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    public int getCantidad(){
        return cantidad;
    }
    
    public int getCantidadDisponible(){
        return cantidad_disponible;
    }
    
    public void setCantidadDisponible(int cantidad_disponible){
        this.cantidad_disponible=cantidad_disponible;
    }

    public int getCantidad_no_endeudada() {
        return cantidad_no_endeudada;
    }

    public void setCantidad_no_endeudada(int cantidad_no_endeudada) {
        this.cantidad_no_endeudada = cantidad_no_endeudada;
    }
    
    public String toString(){
        String t= super.toString();
        t+="Tipo: Objeto\n";
        t+="Cantidad: "+this.cantidad+"\n";
        t+="Cantidad disponible: "+this.cantidad_disponible+"\n";
        return t;
    }
}
