/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.ArrayList;

/**
 *
 * @author jaumesegarra
 */
public class GrupoRegalo {
    private int id;
    private Carrera carrera;
    private float periodo;
    private ArrayList<Regalo> regalos;
    
    public GrupoRegalo(int id, Carrera carrera, float periodo){
        this.id=id;
        this.carrera=carrera;
        this.periodo=periodo;
        regalos= new ArrayList<Regalo>();
    }
    
    protected GrupoRegalo(int id, Carrera carrera, float periodo, ArrayList<Regalo> regalos){
        this.id=id;
        this.carrera=carrera;
        this.periodo=periodo;
        this.regalos=regalos;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
    public int getId(){
        return id;
    }
    
    public float getPeriodo(){
        return periodo;
    }
    
    public ArrayList<Regalo> getRegalos(){
        return regalos;
    }
    
    public Carrera getCarrera(){
        return this.carrera;
    }
    
    public void addRegalo(Regalo reg){
        this.regalos.add(reg);
    }
        
    public String toString(){
        String ts = "";
        ts += "Id: "+this.id+"\n";
        ts += "Periodo: "+this.periodo+"\n";
        ts += "Regalos:\n";
        for(Regalo reg: this.regalos)
            ts += reg.toString();
        return ts;
    }
}
