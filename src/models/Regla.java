/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author jaumesegarra
 */
public class Regla {
    private int id;
    private String nombre;
    private String descripcion;
    
    public Regla(int id, String nombre, String descripcion){
        this.id=id;
        this.nombre=nombre;
        this.descripcion= descripcion;
    }

    public void setId(int id) {
        this.id = id;
    }
   
    public int getId(){
        return id;
    }
    
    public String getNombre(){
        return nombre;
    }
    
    public String getDescripcion(){
        return descripcion;
    }
    
    public String toString(){
        String t= "";
        t+="Nombre: "+this.nombre+"\n";
        t+="Descripción: "+this.descripcion+"\n";
        return t;
    }
}
