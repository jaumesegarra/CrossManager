/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import models.Persona;
import utils.BDconnect;

/**
 * Persona DAO
 * @version 0.1
 * @see Persona
 * @author Jaume Segarra
 */
public class PersonaDAO {
    
    /**
     * Cargar datos de una persona
     * @param dni DNi de la persona
     * @return Datos de la persona
     */
    public static Persona load(String dni){
        return PersonaDAO.load(dni, null);
    }
    
    /**
     * Cargar datos de una persona
     * @param dni DNi de la persona
     * @param con Conexión a la BD
     * @return Datos de la persona
     */
    protected static Persona load(String dni, Connection con){
        Persona p = null;
        boolean new_con = true;
        
        if(con !=  null)
            new_con = false;
        else
            con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            String query = "SELECT * FROM Persona WHERE dni='"+dni+"'";
            
            ResultSet rs = st.executeQuery(query);
            
            if (rs.next())
            {
                p = new Persona(rs.getString("dni"), rs.getString("nombre"), rs.getString("apellidos"), rs.getDate("fecha_nacimiento"), rs.getString("direccion"), rs.getInt("telefono"), rs.getString("sexo"));
            }
            
            st.close();
            
            if(new_con)
                con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return p;
    }
    
    /**
     * Guardar una nueva persona o actualizar una ya existente
     * @param pers Persona a guardar
     * @param con Conexión a la BD
     * @return datos de la persona
     */
    public static boolean save(Persona pers, Connection con){
        boolean saved=false;
        boolean commit_ = false;
        if (con != null)
            commit_=true;
        else
            con = BDconnect.connect();
        
        try{
            if(commit_)
                con.setAutoCommit(false);
            
            /*
                Compprobar si exsite la persona en la BD
            */
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Persona WHERE dni='"+pers.getDni()+"'");
            
            int exists = 0;
            while (rs.next()) {
                exists = rs.getInt(1);
            }
            rs.close();
            
            if(exists==0){//si no existe
                java.sql.Date javaSqlDate = new java.sql.Date(pers.getFecha_nacimiento().getTime());
                
                Statement stmt_cPersona = con.createStatement();
                stmt_cPersona.executeUpdate("INSERT INTO Persona (dni, nombre, apellidos, fecha_nacimiento, direccion, telefono, sexo) VALUES ('"+pers.getDni()+"', '"+pers.getNombre()+"', '"+pers.getApellidos()+"', '"+javaSqlDate+"', '"+pers.getDireccion()+"', "+((pers.getTelefono()>0 && (pers.getTelefono()+"").length() == 9) ? pers.getTelefono() : "NULL")+", '"+pers.getSexo()+"')");
                stmt_cPersona.close();
            }else{ //si existe
                Statement stmt_aPersona = con.createStatement();
                stmt_aPersona.executeUpdate("UPDATE Persona SET nombre='"+pers.getNombre()+"', apellidos='"+pers.getApellidos()+"', direccion='"+pers.getDireccion()+"', telefono="+((pers.getTelefono()>0 && (pers.getTelefono()+"").length() == 9) ? pers.getTelefono() : "NULL")+" WHERE dni='"+pers.getDni()+"'");
                stmt_aPersona.close();
            }
            
            saved=true;
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            
            try{
                if (con != null) {
                    if(commit_){
                        con.rollback();
                        con.setAutoCommit(true);
                    }
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return saved;
    }
    
    /**
     * Eleminar a una persona
     * @param dni Dni de la persona
     * @param con Conexión a la BD
     * @return True si se eliminó, False si no
     */
    protected static boolean delete(String dni, Connection con){
        boolean deleted = false;
        try{
            Statement stmt_rPersona = con.createStatement();
            stmt_rPersona.executeUpdate("DELETE FROM Persona WHERE dni='"+dni+"'");
            stmt_rPersona.close();
            
            deleted = true;
        }catch(SQLException e){
            BDconnect.showMYSQLerrors(e);
            
            try{
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return deleted;
    }
}
