/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Aportacion;
import models.AportacionDinero;
import models.AportacionObjeto;
import models.Patrocinador;
import utils.BDconnect;

/**
 * Aportación DAO
 * @version 0.1
 * @see Aportacion
 * @author Jaume Segarra
 */
public class AportacionDAO {
    
    /**
     * Cargar datos de una aportación
     * @param id Id de la aportación
     * @return Datos de la aportación
     */
    public static Aportacion load(int id){
        Aportacion ap = null;
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM DonacionObjeto WHERE idDonacion="+id); // Consultar si la donación ds de tipo objeto

            int exists = 0;
            if (rs.next()) {
                exists = rs.getInt(1);
            }
            rs.close();
            
            if(exists>0) // Si es de tipo objeto
                ap = AportacionObjetoDAO.load(id, con); // Cargar datos Aportación de tipo objeto
            else if(exists==0) // Si no es de tipo dinero
                ap = AportacionDineroDAO.load(id, con); // Cargar datos Aportación de tipo dinero
            
            con.close(); // Cerrar conexión con la BD
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return ap;
    }
    
    /**
     * Cargar datos de todas las aportaciones
     * @return Array con las aportaciones
     */
    public static ArrayList<Aportacion> loadAll(){
        ArrayList<Aportacion> al = new ArrayList<Aportacion>();
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try{
            Statement st = con.createStatement();
            
            ResultSet rs = st.executeQuery("SELECT d.*, dd.cantidad AS cantidadD, doo.cantidad AS cantidadO, doo.cantidad_disponible FROM Donacion d LEFT JOIN DonacionObjeto doo ON d.id = doo.idDonacion LEFT JOIN DonacionDinero dd ON d.id = dd.idDonacion;"); // Consultar datos de las aportaciones con una columna para cada tipo de cantidad posible (INT = Objeto, FLOAT= Dinero)
            
            while (rs.next()) //Recorrer resultado consulta
            {
                Patrocinador aportador = PatrocinadorDAO.load(rs.getString("nombrePatrocinador"), con);
                
                if(rs.getString("cantidadD")!=null){ // Si la columna "cantidadD" no esta vacia, la aportación es de tipo Dinero
                    al.add(new AportacionDinero(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getFloat("cantidadD"))); // Añadir al array la aportación de tipo dinero
                }else if(rs.getString("cantidadO")!=null){ // Si la columna "cantidadO" no esta vacia, la aportación es de tipo Objeto
                    AportacionObjeto apo = new AportacionObjeto(rs.getInt("id"), rs.getString("concepto"), aportador, rs.getInt("cantidadO"), rs.getInt("cantidad_disponible")); // Cargar datos de la aportación de tipo objeto
                    
                    apo.setCantidad_no_endeudada(AportacionObjetoDAO.calculateNE(apo, con)); // Calcular No endeudado
                    
                    al.add(apo); // Añadir aportacion al Array
                }
            }
             
            rs.close(); // Cerrar query
            con.close(); // Cerrar conexión con la BD 
        } catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
                
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return al;
    }
    
    /**
     * Guardar una nueva aportación o actualizar una ya existente
     * @param ap Aportación a guardar
     * @param con Conexión a la BD: Solo se pasa si se quiere utilizar con un commit existente
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Aportacion ap, Connection con){
        boolean saved = false;
        boolean commit_ = false;
        if (con != null) // Si la conexión pasada no es NULL
            commit_=true;
        else
            con = BDconnect.connect(); // Creamos una nuevo conexión
                
        try {      
            if(commit_) // Si es un commit
                con.setAutoCommit(false); // Deshabilitar el AUTOCOMMIT de MYSQL
            
            if(ap.getId()==0){ //La aportación no existe en la BD (Almacenar)
                String query = "INSERT INTO Donacion (concepto, nombrePatrocinador)"
                + " values (?, ?)";

                PreparedStatement preparedStmt = con.prepareStatement(query);
                preparedStmt.setString (1, ap.getConcepto());
                preparedStmt.setString (2, ap.getAportador().getNombre());
                preparedStmt.execute(); // Guardar datos aportación
                preparedStmt.closeOnCompletion(); // Cerrar query al completar
                
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id) FROM Donacion"); // Obtener Id de la aportación
                if(rs.next()){
                    ap.setId(rs.getInt(1)); // Cargar Id en la aportación
                }
                rs.close(); // Cerrar query
                
            }else{
                Statement st_actualizarDatosAportacion = con.createStatement();
                String query2 = "UPDATE Donacion SET concepto='"+ap.getConcepto()+"' WHERE id="+ap.getId();
                st_actualizarDatosAportacion.executeUpdate(query2); // Actualizar datos de la aportación
                st_actualizarDatosAportacion.close(); // Cerrar query
            }
            
            saved=true; // Marcar como correcta
            
        } catch (SQLException ex) {
            saved = false; // Marcar como fallida
            
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) { // Si no se ha cerrado la conexión
                    if(commit_) // Si es un commit
                        con.rollback(); // Hacer rollback de los querys
                    con.setAutoCommit(true); // Habilitar el AUTOCOMMIT de MYSQL
                    con.close(); // Cerrar conexión
                }
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return saved;
    }
    
    /**
     * Eliminar una aportación
     * @param id Id de la aportación
     */
    public static void delete(int id){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try{
            Statement st_eliminarAportacion = con.createStatement();
            String query = "DELETE FROM Donacion WHERE id="+id;
            st_eliminarAportacion.executeUpdate(query); // Eliminar aportación
            st_eliminarAportacion.close(); // Cerrar query
            
            con.close(); // Cerrar conexión con la BD
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
                
            } catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
}
