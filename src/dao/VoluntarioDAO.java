/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Persona;
import models.Voluntario;
import utils.BDconnect;

/**
 * Voluntario DAO
 * @version 0.1
 * @see Voluntario
 * @author Jaume Segarra
 */
public class VoluntarioDAO {
    
    /**
     * Comprobar si existe un Voluntario en la BD
     * @param dni Dni del voluntario
     * @param con Conexión a la BD
     * @return True si existe, False si no
     */
    protected static boolean exists(String dni, Connection con){
        boolean exists_=false;
        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Voluntario WHERE dniPersona='"+dni+"'");

            int exists = 0;
                while (rs.next()) {
                    exists = rs.getInt(1);
                }
            rs.close();
            
            if(exists > 0) exists_=true;
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return exists_;
    }
    
    /**
     * Cargar puestos de un voluntario
     * @param dni Dni del voluntario
     * @param con Conexión a la BD
     * @return Array con los puestos
     */
    private static ArrayList<String[]> loadPuestosVoluntario(String dni, Connection con){
        ArrayList<String[]> puestos = new ArrayList<String[]>();
                
        try{
            Statement st_puestos = con.createStatement();
            String query_puestos = "SELECT * FROM PuestoVoluntario WHERE dniPersonaVoluntario='"+dni+"'";
            ResultSet rs_puestos = st_puestos.executeQuery(query_puestos);

            while(rs_puestos.next()){
                puestos.add(new String[]{rs_puestos.getString("puesto"), rs_puestos.getString("cometido")});
            }
            
            rs_puestos.close();
            st_puestos.close();
        }catch(SQLException e){
            BDconnect.showMYSQLerrors(e);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }       
        
        return puestos;
    }
    
    /**
     * Cargar todos los datos de un voluntario, hasta sus datos personales
     * @param dni Dni
     * @return Datos del corredor
     */
    public static Voluntario load(String dni){
        Persona pers = PersonaDAO.load(dni);
        return VoluntarioDAO.load(pers);
    }
    
    /**
     * Cargar datos del voluntario a partir de sus datos personales
     * @param pers Datos personales
     * @return Datos del voluntario con los datos personales pasados
     */
    public static Voluntario load(Persona pers){
        Voluntario v = null;
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            String query = "SELECT * FROM Voluntario WHERE dniPersona='"+pers.getDni()+"'";
            
            ResultSet rs = st.executeQuery(query);
            
            if (rs.next())
            {
                ArrayList<String[]> puestos = loadPuestosVoluntario(pers.getDni(), con); //Obtener puestos del voluntario
                
                v = new Voluntario(pers.getDni(), pers.getNombre(), pers.getApellidos(), pers.getFecha_nacimiento(), pers.getDireccion(), pers.getTelefono(), pers.getSexo(), puestos);
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return v;
    }
    
    /**
     * Cargar datos de todos los voluntarios
     * @return Array con los voluntarios
     */
    public static ArrayList<Voluntario> loadAll(){
        ArrayList<Voluntario> cs = new ArrayList<Voluntario>();
        
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            String query = "SELECT * FROM Voluntario";
            
            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                ArrayList<String[]> puestos = loadPuestosVoluntario(rs.getString("dniPersona"), con); //cargar puestos del voluntario
                
                Persona pers = PersonaDAO.load(rs.getString("dniPersona"), con); //cargar datos personales del voluntario
                
                cs.add(new Voluntario(pers.getDni(), pers.getNombre(), pers.getApellidos(), pers.getFecha_nacimiento(), pers.getDireccion(), pers.getTelefono(), pers.getSexo(), puestos));
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return cs;
    }
           
    /**
     * Guardar un nuevo voluntario o actualizar uno ya existente
     * @param vol Voluntario a guardar
     * @return True si se guardó, False si no
     */
    public static boolean save(Voluntario vol){
        Connection con = BDconnect.connect();
        
        boolean saved = PersonaDAO.save(vol, con); //transaccion
        boolean send_commit = true; 
        
        if(saved){
            try{
                if(!exists(vol.getDni(), con)){ //Si no esta registrado como voluntario
                    Statement stmt_cVoluntario = con.createStatement();
                    stmt_cVoluntario.executeUpdate("INSERT INTO Voluntario (dniPersona) VALUES ('"+vol.getDni()+"')");
                    stmt_cVoluntario.close();
                }else{
                    Statement stmt_ePuestosVoluntario = con.createStatement();
                    stmt_ePuestosVoluntario.executeUpdate("DELETE FROM PuestoVoluntario WHERE dniPersonaVoluntario='"+vol.getDni()+"'");
                    stmt_ePuestosVoluntario.close(); //eliminar puestos antiguos voluntario
                }
                
                for(String[] ps : vol.getPuestos()){
                    Statement stmt_aPuestosVoluntario = con.createStatement();
                    stmt_aPuestosVoluntario.executeUpdate("INSERT INTO PuestoVoluntario (dniPersonaVoluntario, puesto, cometido) VALUES ('"+vol.getDni()+"','"+ps[0]+"','"+ps[1]+"')");
                    stmt_aPuestosVoluntario.close(); // insertar puestos del voluntario
                }
                
                if(send_commit){
                    saved=true;
                    con.commit();
                }else{
                    saved=false;
                    con.rollback();
                }
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
            }
        }
        
        return saved;
    }
    
    /**
     * Eliminar a un voluntario
     * @param dni Dni del voluntario
     * @return True si se eliminó, False si no
     */
    public static boolean delete(String dni){
        boolean deleted = false;
        Connection con = BDconnect.connect();
        
        try{
            Statement stmt_rCorredor = con.createStatement();
            stmt_rCorredor.executeUpdate("DELETE FROM Voluntario WHERE dniPersona='"+dni+"'");
            stmt_rCorredor.close();
            
            /*
                Si no es corredor, eliminar sus datos personales
            */
            if(!CorredorDAO.exists(dni, con)) 
               PersonaDAO.delete(dni, con);
            
            con.close();
        }catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) 
                        con.close();
                    
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
        }
        return deleted;
    }
}
