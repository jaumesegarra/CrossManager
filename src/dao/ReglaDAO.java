/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import models.Regla;
import utils.BDconnect;

/**
 * Regla DAO
 * @version 0.1
 * @see Regla
 * @author Jaume Segarra
 */
public class ReglaDAO {
    
    /**
     * Cargar datos de una regla dado su id
     * @param id Id de la regla
     * @return Datos de la regla
     */
    public static Regla load(int id){
        Regla r = null;
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Regla WHERE id='"+id+"'"); // Consultar datos regla con id 
            if(rs.next()) //Recorrer resultado consulta
                r  = new Regla(rs.getInt("id"), rs.getString("nombre"),rs.getString("descripcion")); // Crea el objeto a devolver con los datos de la regla
            
            st.close(); // Cerrar query
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return r;
    }
    
    /**
     * Cargar reglas de una carrera
     * @param categoria Categoría de la carrera
     * @param sexo Sexo de la carrera
     * @return Array con las reglas
     */
    protected static ArrayList<Regla> loadFromCarrera(String categoria, String sexo, Connection con){
        ArrayList<Regla> aR = new ArrayList<Regla>();
        
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Regla WHERE id IN(SELECT idRegla FROM ReglaCarrera WHERE categoriaCarrera='"+categoria+"' AND sexoCarrera='"+sexo+"')"); // Consultar datos de las reglas de la carrera 
            
            while(rs.next()) //Recorrer resultado consulta
                aR.add(new Regla(rs.getInt("id"), rs.getString("nombre"),rs.getString("descripcion"))); // Añadir regla al Array
            
            st.close(); // Cerrar query
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return aR;
    }
    
    /**
     * Cargar todas las reglas
     * @return Array con las reglas
     */
    public static ArrayList<Regla> loadAll(){
        ArrayList<Regla> aR = new ArrayList<Regla>();
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM Regla"); // Consultar datos de las reglas
            
            while(rs.next()) //Recorrer resultado consulta
                aR.add(new Regla(rs.getInt("id"), rs.getString("nombre"),rs.getString("descripcion"))); //Añadir regla al Array
            
            st.close(); // Cerrar query
            con.close(); //  Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return aR;
    }
    
    /**
     * Guardar una nueva regla o actualizar una ya existente
     * @param reg Regla a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Regla reg){
        boolean saved = false;
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        
        try {            
            if(reg.getId()==0){ //La regla no existe en la BD (Almacenar)
                String query = "INSERT INTO Regla (nombre, descripcion)"
                + " values (?, ?)";

                PreparedStatement preparedStmt = con.prepareStatement(query);
                preparedStmt.setString (1, reg.getNombre());
                preparedStmt.setString (2, reg.getDescripcion());
                preparedStmt.execute(); // Guardar datos de la regla
                preparedStmt.close(); // Cerrar query
                
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery("SELECT MAX(id) FROM Regla"); // Obtener Id de la regla
                if(rs.next())
                    reg.setId(rs.getInt(1)); // Asignar Id a la regla
                st.close(); // Cerrar query

            }else{ // La carrera ya existe en la BD (Actualizar)
                Statement st_actualizarDatosRecorrido = con.createStatement();
                String query2 = "UPDATE Regla SET nombre='"+reg.getNombre()+"', descripcion='"+reg.getDescripcion()+"' WHERE id="+reg.getId();
                st_actualizarDatosRecorrido.executeUpdate(query2); // Actualizar datos de la regla
                st_actualizarDatosRecorrido.close(); // Cerrar query
            }
            
            saved=true; // Marcar como correcta
            con.close(); // Cerrar conexión con la BD
        } catch (SQLException ex) {
            saved=false; // Marcar como fallida
            
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        System.out.println(reg.toString()); // Mostrar datos de la regla en la terminal
        
        return saved;
    }
    
    /**
     * Eliminar una regla
     * @param id Id de la regla
     */
    public static void delete(int id){
        Connection con = BDconnect.connect(); // Establecer conexión con la BD
        try{
            Statement st_eliminarRegla = con.createStatement();
            String query = "DELETE FROM Regla WHERE id="+id;
            st_eliminarRegla.executeUpdate(query); // Eliminar regla
            st_eliminarRegla.close(); // Cerrar query
            
            con.close(); // Cerrar conexión
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex); //Muestra los errores SQL
            
            try{
                if (con != null) // Si no se ha cerrado la conexión
                    con.close(); // la cierra
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
    }
}
