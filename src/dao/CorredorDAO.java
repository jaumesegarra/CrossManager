/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import models.Carrera;
import models.Club;
import models.Corredor;
import models.Persona;
import utils.BDconnect;
import utils.Check;

/**
 * Corredor DAO
 * @version 0.1
 * @see Corredor
 * @author Jaume Segarra
 */
public class CorredorDAO {
    
    /**
     * Obtener el dni de un corredor a partir de su dorsal
     * @param dorsal Dorsal del corredor
     * @return Dni
     */
    public static String obtainDNI(int dorsal){
        String d = null;
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            String query = "SELECT dniPersona FROM Corredor WHERE dorsal="+dorsal;
            
            ResultSet rs = st.executeQuery(query);
            
            if (rs.next())
            {
                d = rs.getString(1); //dni
            }
            
            rs.close();
            
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return d;
    }
    
    /**
     * Comprobar si existe un Corredor en la BD
     * @param dni Dni del corredor
     * @param con Conexión a la BD
     * @return True si existe, False si no
     */
    protected static boolean exists(String dni, Connection con){
        boolean exists_=false;
        try{
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT COUNT(*) FROM Corredor WHERE dniPersona='"+dni+"'");

            int exists = 0;
                while (rs.next()) {
                    exists = rs.getInt(1);
                }
            rs.close();
            
            if(exists > 0) exists_=true;
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return exists_;
    }
    
    /**
     * Cargar todos los datos de un corredor, hasta sus datos personales
     * @param dni Dni
     * @return Datos del corredor
     */
    public static Corredor load(String dni){
        Persona pers = PersonaDAO.load(dni);
        return CorredorDAO.load(pers);
    }
    
    /**
     * Cargar datos del corredor a partir de sus datos personales
     * @param pers Datos personales
     * @return Datos del corredor con los datos personales pasados
     */
    public static Corredor load(Persona pers){
        Corredor c = null;
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            String query = "SELECT c.*, ce.edad FROM Corredor c, CorredorEdadVista ce WHERE ce.dni=c.dniPersona AND ce.dni='"+pers.getDni()+"'";
            
            ResultSet rs = st.executeQuery(query);
            
            if (rs.next())
            {
                Club club_ = null;
                if(rs.getInt("idClub") != 0)
                    club_ = ClubDAO.load(rs.getInt("idClub"), con);
                    
                c = new Corredor(pers.getDni(), pers.getNombre(), pers.getApellidos(), pers.getFecha_nacimiento(), pers.getDireccion(), pers.getTelefono(), pers.getSexo(), rs.getInt("edad"), club_, rs.getInt("dorsal"), rs.getInt("puesto_llegada"));
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        return c;
    }
    
    /**
     * Cargar todos los corredores de una determinada carrera
     * @param categoria Categoria de la carrera
     * @param sexo Sexo de la carrera
     * @return Array con los corredores
     */
    public static ArrayList<Corredor> loadAll(String categoria, String sexo){
        return CorredorDAO.loadAll(categoria, sexo, false);
    }
    
    /**
     * Cargar todos los corredores de una determinada carrera o la clasificación (quitando a los que no hayan retirado su dorsal aún y ordenandolos por posición)
     * @param categoria Categoria de la carrera
     * @param sexo Sexo de la carrera
     * @param clasificacion True= Clasificacion
     * @return Array con los datos de los corredores
     */
    public static ArrayList<Corredor> loadAll(String categoria, String sexo, boolean clasificacion){
        ArrayList<Corredor> cs = new ArrayList<Corredor>();
        
        Connection con = BDconnect.connect();
        
        try {
            Statement st = con.createStatement();
            
            String query = "SELECT c.*, p.sexo, ce.edad FROM Persona p, Corredor c, CorredorEdadVista ce WHERE p.dni=ce.dni AND c.dniPersona=ce.dni AND ";
            int[] edad=Carrera.getEdadParticipar(categoria, sexo);
            if(edad[0] != -1)
                query+= "edad>="+edad[0]+" AND ";
            if(edad[1] != -1)
                query+= "edad<="+edad[1]+" AND ";
            query+="sexo='"+sexo+"'";
            
            if(clasificacion)
                query+=" AND puesto_llegada IS NOT NULL ORDER BY puesto_llegada ASC";
            
            
            ResultSet rs = st.executeQuery(query);
            
            while (rs.next())
            {
                Club club_ = null;
                if(rs.getInt("idClub") != 0)
                    club_ = ClubDAO.load(rs.getInt("idClub"), con);
                
                Persona pers = PersonaDAO.load(rs.getString("dniPersona"), con);
                
                cs.add(new Corredor(pers.getDni(), pers.getNombre(), pers.getApellidos(), pers.getFecha_nacimiento(), pers.getDireccion(), pers.getTelefono(), pers.getSexo(), rs.getInt("edad"), club_, rs.getInt("dorsal"), rs.getInt("puesto_llegada")));
            }
            
            st.close();
            con.close();
        }catch (SQLException ex) {
            BDconnect.showMYSQLerrors(ex);
            try{
                if (con != null) 
                    con.close();
            }catch (SQLException ex1) {
                BDconnect.showMYSQLerrors(ex1);
            }
        }
        
        return cs;
    }
    
    /**
     * Guardar datos de un nuevo corredor o actualizar uno ya existente
     * @param cor Datos del corredor a guardar
     * @return True si se guardó, False si hubo un error
     */
    public static boolean save(Corredor cor){
        Connection con = BDconnect.connect();
        
        boolean saved = PersonaDAO.save(cor, con); // Guardar o actualizar datos personales del corredor
        boolean send_commit = true;
        
        if(saved){
            try{                
                String club = (cor.getClub() != null) ? cor.getClub().getId()+"" : "NULL";
                if(!exists(cor.getDni(), con)){
                        
                    Statement stmt_cCorredor = con.createStatement();
                    stmt_cCorredor.executeUpdate("INSERT INTO Corredor (dniPersona, idClub) VALUES ('"+cor.getDni()+"',"+club+")");
                    stmt_cCorredor.close();
                    
                    /*
                        Obtener edad y dorsal de la BD
                    */
                    Statement stGetDorsal = con.createStatement();
                    ResultSet rsGetDorsal = stGetDorsal.executeQuery("SELECT c.dorsal, ce.edad FROM Corredor c, CorredorEdadVista ce WHERE c.dniPersona='"+cor.getDni()+"' AND ce.dni='"+cor.getDni()+"'");
                    while (rsGetDorsal.next()) {
                        cor.setDorsal(rsGetDorsal.getInt(1));
                        cor.setEdad(rsGetDorsal.getInt(2));
                    }
                    rsGetDorsal.close();
                    
                    String[] carrera_participa = cor.getCarrera();
                    
                    /*
                        Comprobar que este registrada la carrera para su categoría y sexo; y que esté aprobada
                    */
                    int dis = 0;
                    Statement stCheckCarrera = con.createStatement();
                    ResultSet rsCheckCarrera = stCheckCarrera.executeQuery("SELECT COUNT(*) FROM Carrera WHERE categoria='"+carrera_participa[0]+"' AND sexo='"+carrera_participa[1]+"' AND aprobada=1");
                    while (rsCheckCarrera.next()) {
                        dis = rsCheckCarrera.getInt(1);
                    }
                    rsCheckCarrera.close();
                    
                    if(dis==0){
                        send_commit =false;
                        JOptionPane.showMessageDialog(null, "La carrera "+carrera_participa[0]+" ("+carrera_participa[1]+") aún no está aprobada.", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    
                    /*
                        Comprobar que no está fuera de plazo la inscripción: hasta media hora antes que empiece la carrera
                    */
                    GregorianCalendar fecha_actual = new GregorianCalendar();
                    Carrera car = CarreraDAO.load(carrera_participa[0], carrera_participa[1], con);
                    String[] hora_comienzo = car.getHoraComienzo().split(":");
                                        
                    if(
                            Check.isEq(fecha_actual, car.getFecha()) && (fecha_actual.get(Calendar.HOUR_OF_DAY)*60+fecha_actual.get(Calendar.MINUTE)+30 >= new Integer(hora_comienzo[0])*60+new Integer(hora_comienzo[1])) ||
                            Check.isGt(fecha_actual, car.getFecha())
                    ){
                            send_commit =false;
                            JOptionPane.showMessageDialog(null, "Ya no es posible inscribirse a la carrera "+carrera_participa[0]+" ("+carrera_participa[1]+").", "Error: Fuera de plazo", JOptionPane.ERROR_MESSAGE);
                    }
                }else{                    
                    Statement stmt_aCorredor = con.createStatement();
                    stmt_aCorredor.executeUpdate("UPDATE Corredor SET idClub="+club+" WHERE dniPersona='"+cor.getDni()+"'"); //Actualizar club del corredor
                    stmt_aCorredor.close();
                }
            
                if(send_commit){ // Si todo ha ido bien
                    saved=true;
                    con.commit(); //hacemos commit
                }else{
                    saved=false;
                    con.rollback();
                }
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
            }
        }
        
        return saved;
    }
    
    /**
     * Guardar posición final del corredor
     * @param dorsal Dorsal del corredos
     * @return Posición final
     */
    public static int savePuestoLlegada(int dorsal){
        int pos = -1; // Posición a devolver (-1 si hubo un error)
        Connection con = BDconnect.connect();
        
        String dni = obtainDNI(dorsal); //Obtenemos el dni del corredor para conocer sus datos posteriormente
        Corredor cor = null;
        if(dni != null) //Si existe un corredor con ese dorsal
            cor = CorredorDAO.load(dni); // Cargamos sus datos
                
        if(cor != null && cor.getPuesto_llegada() == 0){ //Si existe el corredor y no ha sido guardado aún su posición
            try{
                con.setAutoCommit(false);

                String[] carrera = cor.getCarrera();

                Statement stCheckPos = con.createStatement();
                String query = "SELECT MAX(c.puesto_llegada) FROM Persona p, Corredor c, CorredorEdadVista ce WHERE p.dni=ce.dni AND c.dniPersona=ce.dni AND ";
                int[] edad=Carrera.getEdadParticipar(carrera[0], carrera[1]);
                if(edad[0] != -1)
                    query+= "edad>="+edad[0]+" AND ";
                if(edad[1] != -1)
                    query+= "edad<="+edad[1]+" AND ";
                query+="sexo='"+cor.getSexo()+"'";
 
                ResultSet rsCheckPos = stCheckPos.executeQuery(query); //Buscamos última posición registrada en la carrera del usuario
                if (rsCheckPos.next()) {
                    pos = rsCheckPos.getInt(1);
                }
                rsCheckPos.close();

                pos++; // Incrementamos la última posición registrada

                Statement stmt_sPosCorredor = con.createStatement();
                stmt_sPosCorredor.executeUpdate("UPDATE Corredor SET puesto_llegada="+pos+" WHERE dniPersona='"+cor.getDni()+"' AND puesto_llegada IS NULL");
                stmt_sPosCorredor.close(); //Guardamos la posición

                con.commit();
                con.setAutoCommit(true);
                con.close();
                
            } catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) {
                        con.rollback();
                        con.setAutoCommit(true);
                        con.close();
                    }
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
            }
        }
        return pos;
    }
    
    /**
     * Eliminar un corredor de la BD, si es voluntario no se eleminarán susu datos personales ni susu datoos de voluntario
     * @param dni Dni del corredor
     * @return True si se eliminó correctamente, False si hubo un error
     */
    public static boolean delete(String dni){
        boolean deleted = false;
        Connection con = BDconnect.connect();
        
        try{
            Statement stmt_rCorredor = con.createStatement();
            stmt_rCorredor.executeUpdate("DELETE FROM Corredor WHERE dniPersona='"+dni+"'");
            stmt_rCorredor.close();
            
            /*
                Si no es voluntario, eliminar sus datos personales
            */
            if(!VoluntarioDAO.exists(dni, con))
                PersonaDAO.delete(dni, con);
            
            
            con.close();
        }catch (SQLException ex) {
                BDconnect.showMYSQLerrors(ex);

                try{
                    if (con != null) 
                        con.close();
                    
                } catch (SQLException ex1) {
                    BDconnect.showMYSQLerrors(ex1);
                }
        }
        return deleted;
    }
}
