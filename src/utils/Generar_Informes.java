/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import dao.AportacionDineroDAO;
import dao.AportacionObjetoDAO;
import dao.CarreraDAO;
import dao.ClubDAO;
import dao.CorredorDAO;
import dao.RecorridoDAO;
import dao.VoluntarioDAO;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import models.Aportacion;
import models.AportacionDinero;
import models.AportacionObjeto;
import models.Carrera;
import models.Club;
import models.Corredor;
import models.Recorrido;
import models.Regla;
import models.Voluntario;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * Genera todos los informes y hace todo lo demás realacionado con ellos
 * @author Jaume Segara
 */

public class Generar_Informes {
    private static final String FOLDER = "informes"; // Carpeta donde se guardan los informes
    
    /**
     * Crea el dialogo que se motrará mientras se generá el informe
     * @param text Titulo a mostrar en el dialogo
     * @return Dialogo
     */
    private static JDialog loadingDialog(String text){
        JDialog dlg = new JDialog(new JFrame(),text, true);
        JProgressBar dpb = new JProgressBar(0, 500);
        dlg.add(BorderLayout.CENTER, dpb);
        dlg.add(BorderLayout.NORTH, new JLabel(text));
        dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        dlg.setSize(300, 75);
        dlg.setLocationRelativeTo(null);
        dlg.setResizable(false);
        
        Thread t = new Thread(new Runnable() {
            public void run() {
              dlg.setVisible(true);
            }
        });
        t.start();
        
        return dlg;
    }
    
    /**
     * Cerrar dialogo
     * @param dlg Dialogo a cerrar
     */
    private static void closeLoadingDialog(JDialog dlg){
        try {
            Thread.sleep(25);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        dlg.dispose();
    }
    
    /**
     * Crear carpeta si no existe
     * @param name Ruta de la carpeta a crear
     */
    private static void _createFolder(String name){
            File ffolder = new File(name+"/");
            if (!ffolder.exists()) {
                try{
                    ffolder.mkdir();
                } 
                catch(SecurityException se){}
            }
    }
    
    /**
     * Obtiene la plantilla HTML para generar los informes
     * @return String con el código de la plantilla
     */
    private static String obtenerPlantillaHTML(){
        String pl = null;
        File f = new File("plantilla_informes.html");
        if(f.exists()){
            pl= "";
            try{
                FileReader fr = new FileReader(f);
                BufferedReader br = new BufferedReader(fr);
                
                String line; 
                while((line = br.readLine()) !=  null){
                    pl+=line+"\r\n";
                }
                
                br.close();
                fr.close();
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        
        return pl;
    }
    
    /**
     * Genera el informe general en formato de texto plano y HTML
     */
    public static void generarInGeneral(){
        JDialog dlg = loadingDialog("Creando informe general...");
        
        _createFolder(FOLDER); //crea la carpeta de informes si no existe aún
        
        final String NOMBRE_ARCHIVO = "general"; //nombre del archivo del informe
        
        try{
            File f = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".txt");
            FileWriter fw = new FileWriter(f);
            
            String html = ""; // codigo HTML del informe
            
            fw.write("##########################################################\r\n");
            fw.write("##\tCARRERA POPULAR Fiestas Barrio del Pilar\t##\r\n");
            fw.write("##########################################################\r\n\r\n");
            
            fw.write("Categorías que participarán y datos de su carrera:\r\n");
            html+= "<label>Categorías que participarán y datos de su carrera:</label>\r\n<ul>\r\n";
            
            ArrayList<Carrera> carreras = CarreraDAO.loadAll();
            
            int n_vrecorrido=0;
            for(Carrera car : carreras){
                fw.write("\t-"+car.getCategoria()+" ("+car.getSexo()+"): "+car.getHoraComienzo()+"h [Recorrido "+car.getRecorrido().getCodigo()+"] "+car.getRecorrido().getKilometros()+" km"+"\r\n");
                html+= "\t<li>"+car.getCategoria()+" ("+car.getSexo()+"): "+car.getHoraComienzo()+"h [Recorrido <a href='#r-"+car.getRecorrido().getCodigo()+"'>"+car.getRecorrido().getCodigo()+"</a>] "+car.getRecorrido().getKilometros()+" km </li>\r\n";
                n_vrecorrido+=car.getRecorrido().getKilometros()/5;
            }
            html+= "</ul>\r\n";
            
            fw.write("\r\nDetalles de los recorridos:\r\n");
            html+= "<label>Detalles de los recorridos:</label>\r\n<ul>\r\n";
            
            for(Recorrido rec : RecorridoDAO.loadAll()){
                fw.write("\t-"+rec.getCodigo()+" ("+rec.getKilometros()+"km): "+rec.getDescripcion()+"\r\n");
                html+= "\t<li id='r-"+rec.getCodigo()+"'>"+rec.getCodigo()+" ("+rec.getKilometros()+"km): "+rec.getDescripcion()+"</li>\r\n";
            }
            html+= "</ul>\r\n";
            
            int n_vsecretaria=3+(2*carreras.size());
            int n_vmegafonia=1*carreras.size();
            int n_vsalida=4*carreras.size();
            int n_vmeta=3*carreras.size();

            int n_vtotal = n_vsecretaria+n_vmegafonia+n_vsalida+n_vmeta+n_vrecorrido;
            fw.write("\r\n\r\nNúmero de voluntarios necesarios ("+n_vtotal+"): \r\n");
            html+= "<label>Número de voluntarios necesarios ("+n_vtotal+"):</label>\r\n<ul>\r\n";
            fw.write("\t-Secretaria: "+n_vsecretaria+" (tres para inscripciones y dos para cada carrera)\r\n");
            html+= "\t<li>Secretaria: "+n_vsecretaria+" (tres para inscripciones y dos para cada carrera)</li>\r\n";
            fw.write("\t-Megafonia: "+n_vmegafonia+" (uno para cada carrera)\r\n");
            html+= "\t<li>Megafonia: "+n_vmegafonia+" (uno para cada carrera)</li>\r\n";
            fw.write("\t-Salida: "+n_vsalida+" (cuatro para cada carrera)\r\n");
            html+= "\t<li>Salida: "+n_vsalida+" (cuatro para cada carrera)</li>\r\n";
            fw.write("\t-Meta: "+n_vmeta+" (tres para cada carrera)\r\n");
            html+= "\t<li>Meta: "+n_vmeta+" (tres para cada carrera)</li>\r\n";
            fw.write("\t-Recorrido: "+n_vrecorrido+" (uno por cada 5km)\r\n");
            html+= "\t<li>Recorrido: "+n_vrecorrido+" (uno por cada 5km)</li>\r\n";
            html+= "</ul>\r\n";
            
            fw.close();
            
            String plantilla_html = obtenerPlantillaHTML();
            if(plantilla_html != null || !plantilla_html.trim().isEmpty()){ //Si la plantilla HTML existe
                plantilla_html = plantilla_html.replace("{{title}}", "Informe general");
                plantilla_html = plantilla_html.replace("{{head_title}}", "Informe general");
                
                plantilla_html = plantilla_html.replace("{{content}}", html);
                
                File f_html = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".html");
                FileWriter fw_html = new FileWriter(f_html);
                
                fw_html.write(plantilla_html);
                
                fw_html.close();
            }
            
            closeLoadingDialog(dlg);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Reeemplaza los tags de Recorrido del informe para el Ayuntamiento
     * @param text Texto con los posibles tags
     * @param rec Datos del recorrido
     * @return Texto con los tags reemplazados
     */
    private static String replaceRecorrido(String text, Recorrido rec){
        return replaceRecorrido(text,rec,"");
    }
    
    /**
     * Reeemplaza los tags de Recorrido del informe para el Ayuntamiento
     * @param text Texto con los posibles tags
     * @param rec Datos del recorrido
     * @param pattern Texto anterior de los tags
     * @return Texto con los tags reemplazados
     */
    private static String replaceRecorrido(String text, Recorrido rec, String pattern){
        text = text.replace("{{"+pattern+"codigo}}", rec.getCodigo())
                .replace("{{"+pattern+"descripcion}}", rec.getDescripcion())
                .replace("{{"+pattern+"kilometros}}", Float.toString(rec.getKilometros()));
        
        if(text.contains("{{"+pattern+"observaciones}}")){
            String regla_p = StringUtils.substringBetween(text, "{{"+pattern+"observaciones}}", "{{/"+pattern+"observaciones}}");
            String obs_text = "";
            
            text = text.replace("{{"+pattern+"observaciones}}", "");
            text = text.replace(regla_p, "");
            
            for(String ob: rec.getObservaciones()){
                obs_text+= regla_p.replace("{{observacion}}", ob);
            }
            text= text.replace("{{/"+pattern+"observaciones}}", obs_text);
        }
        
        return text;
    }
    
    /**
     * Reeemplaza los tags de Carrera del informe para el Ayuntamiento
     * @param text Texto con los posibles tags
     * @param rec Datos del recorrido
     * @return Texto con los tags reemplazados
     */
    private static String replaceCarrera(String text, Carrera car){
        return replaceCarrera(text,car,"");
    }
    
    /**
     * Reeemplaza los tags de Carrera del informe para el Ayuntamiento
     * @param text Texto con los posibles tags
     * @param rec Datos del recorrido
     * @param pattern Texto anterior de los tags
     * @return Texto con los tags reemplazados
     */
    private static String replaceCarrera(String text, Carrera car, String pattern){
        text = text.replace("{{"+pattern+"categoria}}", car.getCategoria())
                .replace("{{"+pattern+"sexo}}", car.getSexo())
                .replace("{{"+pattern+"hora_comienzo}}", car.getHoraComienzo());
        text = replaceRecorrido(text,car.getRecorrido(),"recorrido.");
        
        if(text.contains("{{"+pattern+"reglas}}")){
            String regla_p = StringUtils.substringBetween(text, "{{"+pattern+"reglas}}", "{{/"+pattern+"reglas}}");
            String reglas_text = "";
            
            text = text.replace("{{"+pattern+"reglas}}", "");
            text = text.replace(regla_p, "");
            
            for(Regla rg : car.getReglas()){
                reglas_text+= regla_p.replace("{{nombre}}", rg.getNombre())
                        .replace("{{descripcion}}", rg.getDescripcion());
            }
            text= text.replace("{{/"+pattern+"reglas}}", reglas_text);
        }
        return text;
    }
    
    /*
        Hay algunos problemas al editar la plantilla:
           En los tags con cierre se pierde el formato (negritas, subrayados,etc..)
    */
    
    
    /**
     * Eliminar un parrafo del docummento
     * @param p Parrafo del documento
     */
    private static void deleteParagraph(XWPFParagraph p) {
        XWPFDocument doc = p.getDocument();
        int pPos = doc.getPosOfParagraph(p);
        doc.getDocument().getBody().removeP(pPos);
    }
    
    /**
     * Generar informe para el Ayuntamiento a partir de la plantilla .docx
     */
    public static void generarInAyuntamiento(){
        JDialog dlg = loadingDialog("Creando informe para el ayuntamiento...");
        
        _createFolder(FOLDER);
        
        File plantilla = new File("plantilla_ayn.docx");
        if(plantilla.exists()){
            XWPFDocument document = null;
            try{
                document = new XWPFDocument(OPCPackage.open(plantilla.getAbsolutePath())); //Leer plantilla
                
                boolean recorridos_for=false; String recorridos_plantilla = "";
                boolean carreras_for=false; String carreras_plantilla = "";
                
                int pNumber = document.getParagraphs().size() -1;
                while(pNumber >= 0){ //Recorrer lineas de abajo hacía arriba
                    XWPFParagraph p = document.getParagraphs().get(pNumber);
                    boolean deleteRun = false;
                    List<XWPFRun> runs = p.getRuns(); //Obtener trozos de la linea
                    
                    if (runs != null) {
                        
                        /*
                            Reemplazar tags con etiquetas de cierre
                        */
                        
                        String text_p = p.getParagraphText(); //Obtener texto de la linea
                        if(text_p.contains("[[recorridos]]")){
                            recorridos_for=false;
                            
                            /*
                                Reemplazar tags del recorrido
                            */
                            String pl_recs = "";
                            for(Recorrido re: RecorridoDAO.loadAll())
                                pl_recs += replaceRecorrido(recorridos_plantilla, re);
                            

                            pl_recs = pl_recs.substring(1); //Quitar salto de linea inicial

                            runs.get(0).setText(pl_recs,0); //Reemplazar trozo 0 de la linea por el parrafo con los datos del recorrido
                            
                            //Eliminar los trozos restantes
                            for(int i=1;i<runs.size();i++)
                                runs.get(i).setText("",0);

                        }else if(text_p.contains("[[/recorridos]]")){
                            recorridos_for=true;
                            deleteRun = true; //Eliminar linea del parrafo
                        }else if(recorridos_for){    
                            recorridos_plantilla=p.getParagraphText()+recorridos_plantilla; //Guardar lineas en una variable

                            if(!text_p.contains("{{observaciones}}") && !text_p.contains("{{/observaciones}}"))
                                recorridos_plantilla="\n"+recorridos_plantilla; //Añadir salto de linea

                            deleteRun = true;
                        }else if(text_p.contains("[[carreras]]")){
                            carreras_for=false;
                            String pl_cars = "";
                            for(Carrera car: CarreraDAO.loadAll()){
                                pl_cars += replaceCarrera(carreras_plantilla, car);
                            }

                            pl_cars = pl_cars.substring(1);

                            runs.get(0).setText(pl_cars,0);
                            for(int i=1;i<runs.size();i++)
                                runs.get(i).setText("",0);

                        }else if(text_p.contains("[[/carreras]]")){
                            carreras_for=true;
                            deleteRun = true;
                        }else if(carreras_for){    
                            carreras_plantilla=p.getParagraphText()+carreras_plantilla;

                            if(!text_p.contains("{{reglas}}") && !text_p.contains("{{/reglas}}"))
                                  carreras_plantilla="\n"+carreras_plantilla;

                            deleteRun = true;
                        }else{
                            /*
                                Reemplazar tags simples: de fecha
                            */

                            
                            //Solucionar un problema que trocea palabras: tags
                            
                            String text_ = ""; //guardar tag completo
                            for (XWPFRun r : runs) {
                                if (r.getText(0) != null){
                                    if (r.getText(0).contains("{") && !r.getText(0).contains("}}")){ //si el tag ha empezado pero no se acaba en el mismo trozo
                                        text_ += r.getText(0); //Guardar trozo en la variable
                                        r.setText("", 0); //Vaciar trozo del documento
                                    }else if(!text_.isEmpty()){
                                        text_ += r.getText(0); //Guardar trozo en la variable

                                        if(text_.contains("{{") && r.getText(0).contains("}}")){ //Si lo guardado ya esta completo
                                            r.setText(text_, 0); //guardamos tag completo en el docummento
                                            text_ = ""; //vaciamos variable
                                        }else //si no
                                            r.setText("", 0); //vaciar trozo del documento
                                    }
                                } 
                            }


                            for (XWPFRun r : runs) { // Recorrer trozos del documento
                                String text = r.getText(0); //obtener texto del trozo
                                if (text != null && !text.isEmpty()) {

                                    if(text.contains("{{dia}}") || text.contains("{{mes}}") || text.contains("{{año}}")) { // si el texto es un tag de fecha
                                        Calendar calendar = new GregorianCalendar();

                                        if(text.contains("{{dia}}")) {
                                            text = text.replace("{{dia}}", Integer.toString(calendar.get(Calendar.DATE)));
                                        }

                                        if(text.contains("{{mes}}")) {
                                            String[] meses = new String[]{"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
                                            text = text.replace("{{mes}}", meses[calendar.get(Calendar.MONTH)]);
                                        }

                                        if(text.contains("{{año}}")) {
                                            text = text.replace("{{año}}", Integer.toString(calendar.get(Calendar.YEAR)));
                                        }
                                        r.setText(text,0); //actualizar trozo nuevo

                                    }
                                }
                            }
                        } 
                        
                        if(deleteRun){
                            deleteParagraph(p); //Eliminar parrafo
                        }
                        
                        pNumber--;

                    }
                }
                
            }catch(IOException e){
                e.printStackTrace();
            } catch (InvalidFormatException ex) {
                Logger.getLogger(Generar_Informes.class.getName()).log(Level.SEVERE, null, ex);
            }
    
            /*
                Escribir nuevo documento
            */
            FileOutputStream out = null;
            try{
                out = new FileOutputStream("informes/ayuntamiento.docx");
                document.write(out);
                out.close();
            }catch(IOException e){}
            
            closeLoadingDialog(dlg);
        }else{
            System.out.println("No existe la plantilla!");
            JOptionPane.showMessageDialog(null, "El fichero 'plantilla_ayn.doc' no figura en la carpeta de la aplicación", "No hay plantilla", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * Abrir la carpeta de informes en el explorador de archivos
     */
    public static void verCarpeta(){
        try{
            File ffolder = new File(FOLDER);
            if(ffolder.exists()) //Si existe la carpeta
                Desktop.getDesktop().open(ffolder);
            else
                JOptionPane.showMessageDialog(null, "Parece que aún no has generado los informes", "Error", JOptionPane.ERROR_MESSAGE);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    /**
     * Generar informe de aportaciones
     */
    public static void generarInAportaciones() {
        JDialog dlg = loadingDialog("Creando informe de aportaciones...");
        
        _createFolder(FOLDER);
        
        final String NOMBRE_ARCHIVO = "aportaciones";
        
        try{
            File f = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".txt");
            FileWriter fw = new FileWriter(f);
            
            String html = "";
            
            fw.write("##########################################################\r\n");
            fw.write("##\tCARRERA POPULAR Fiestas Barrio del Pilar\t##\r\n");
            fw.write("##########################################################\r\n\r\n");
            ArrayList<Aportacion> aportacionesObjeto = AportacionObjetoDAO.loadAll();
            ArrayList<Aportacion> aportacionesDinero = AportacionDineroDAO.loadAll();
            
            fw.write("Número de aportaciones total: "+(aportacionesObjeto.size()+aportacionesDinero.size())+"\r\n\r\n\r\n");
            html += "<label>Número de aportaciones total: </label> <span class='number'>"+(aportacionesObjeto.size()+aportacionesDinero.size())+"</span> <br/> <br/>\r\n";
            
            fw.write("Aportaciones materiales ("+aportacionesObjeto.size()+"):\r\n");
            html += "<label>Aportaciones materiales: </label> <span class='number'>"+aportacionesObjeto.size()+"</span>\r\n";
            
            fw.write("-------------------------------------\r\n");
            
            html+="<table border='1'>\r\n\t<tr>\r\n\t\t<th>Concepto</th>\r\n\t\t<th>Aportador</th>\r\n\t\t<th>Cantidad total</th>\r\n\t\t<th>Cantidad disponible</th>\r\n\t<tr/>\r\n";
            
            for(Aportacion ap : aportacionesObjeto){
                fw.write("Concepto: "+ap.getConcepto()+"\r\n");
                fw.write("Aportador: "+ap.getAportador().getNombre()+"\r\n");
                fw.write("Cantidad Total: "+((AportacionObjeto)ap).getCantidad()+"\r\n");
                fw.write("Cantidad Disponible: "+((AportacionObjeto)ap).getCantidadDisponible()+"\r\n\r\n");
                
                html+="\t<tr>\r\n\t\t<td>"+ap.getConcepto()+"</td>\r\n\t\t<td>"+ap.getAportador().getNombre()+"</td>\r\n\t\t<td>"+((AportacionObjeto)ap).getCantidad()+"</td>\r\n\t\t<td>"+((AportacionObjeto)ap).getCantidadDisponible()+"</td>\r\n\t<tr/>\r\n";
            }
            
            html +="</table>\r\n";
            
            fw.write("\r\n\r\n");
            
            fw.write("Aportaciones monetarias ("+aportacionesDinero.size()+"):\r\n");
            html += "<br/><br/><label>Aportaciones monetarias: </label> <span class='number'>"+aportacionesDinero.size()+"</span>\r\n";
            
            fw.write("-------------------------------------\r\n");
            html+="<table border='1'>\r\n\t<tr>\r\n\t\t<th>Concepto</th>\r\n\t\t<th>Aportador</th>\r\n\t\t<th>Cantidad</th>\r\n\t<tr/>\r\n";
            for(Aportacion ap : aportacionesDinero){
                fw.write("Concepto: "+ap.getConcepto()+"\r\n");
                fw.write("Aportador: "+ap.getAportador().getNombre()+"\r\n");
                fw.write("Cantidad: "+((AportacionDinero)ap).getCantidad()+"\r\n\r\n");
                
                html+="\t<tr>\r\n\t\t<td>"+ap.getConcepto()+"</td>\r\n\t\t<td>"+ap.getAportador().getNombre()+"</td>\r\n\t\t<td>"+((AportacionDinero)ap).getCantidad()+"</td>\r\n\t<tr/>\r\n";
            }
            html +="</table>\r\n";
            
            fw.close();
            
            String plantilla_html = obtenerPlantillaHTML();
            if(plantilla_html != null || !plantilla_html.trim().isEmpty()){
                plantilla_html = plantilla_html.replace("{{title}}", "Informe de aportaciones");
                plantilla_html = plantilla_html.replace("{{head_title}}", "Informe de aportaciones");
                
                plantilla_html = plantilla_html.replace("{{content}}", html);
                
                File f_html = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".html");
                FileWriter fw_html = new FileWriter(f_html);
                
                fw_html.write(plantilla_html);
                
                fw_html.close();
            }
            
            closeLoadingDialog(dlg);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generar informe de voluntarios
     */
    public static void generarInVoluntarios() {
        JDialog dlg = loadingDialog("Creando informe de voluntarios...");
        
        _createFolder(FOLDER);
        
        final String NOMBRE_ARCHIVO = "voluntarios";
        
        try{
            File f = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".txt");
            FileWriter fw = new FileWriter(f);
            
            String html = "";
            
            fw.write("##########################################################\r\n");
            fw.write("##\tCARRERA POPULAR Fiestas Barrio del Pilar\t##\r\n");
            fw.write("##########################################################\r\n\r\n");
            
            ArrayList<Voluntario> voluntarios = VoluntarioDAO.loadAll();
            
            fw.write("Número de voluntarios: "+voluntarios.size()+"\r\n");
            html += "<label>Número de voluntarios: </label> <span class='number'>"+voluntarios.size()+"</span>\r\n";
            
            fw.write("-------------------------------------\r\n");
            
            html+="<table border='1'>\r\n\t<tr>\r\n\t\t<th>DNI</th>\r\n\t\t<th>Nombre</th>\r\n\t\t<th>Apellidos</th>\r\n\t\t<th>Sexo</th>\r\n\t\t<th>Fecha nacimiento</th>\r\n\t\t<th>Contacto</th>\r\n\t\t<th>Puestos desempeño</th>\r\n\t<tr/>\r\n";
            
            for(Voluntario c : voluntarios){
                fw.write("DNI: "+c.getDni()+"\r\n");
                fw.write("Nombre: "+c.getNombre()+"\r\n");
                fw.write("Apellidos: "+c.getApellidos()+"\r\n");
                fw.write("Sexo: "+c.getSexo()+"\r\n");
                fw.write("Fecha nacimiento: "+c.getFecha_nacimiento()+"\r\n");
                fw.write("Dirección: "+c.getDireccion()+"\r\n");      
                fw.write("Teléfono: "+((c.getTelefono() != 0) ? c.getTelefono(): "--")+"\r\n");     
                 
                
                html+="\t<tr>\r\n\t\t<td>"+c.getDni()+"</td>\r\n\t\t<td>"+c.getNombre()+"</td>\r\n\t\t<td>"+c.getApellidos()+"</td>\r\n\t\t<td>"+c.getSexo()+"</td>\r\n\t\t<td>"+c.getFecha_nacimiento()+"</td>\r\n\t\t<td>"+c.getDireccion()+((c.getTelefono() != 0) ? "<hr/>"+c.getTelefono(): "")+"</td>\r\n\t\t<td>\r\n\t\t<ul>";
                
                String puestos= ""; int i = 0;
                while(i<c.getPuestos().size()){
                    String[] puesto = c.getPuestos().get(i);
                    puestos+= puesto[0] + ((i != c.getPuestos().size()-1) ? ", " : "");
                    html += "\r\n\t\t\t<li>"+puesto[0]+": <font class='small'>"+puesto[1]+"</font></li>";
                    i++;
                }
                
                html+="\r\n\t\t</ul></td>\r\n\t<tr/>\r\n";
                fw.write("Puestos: "+puestos+"\r\n");  
                
                fw.write("\r\n\r\n");
            }
            
            html +="</table>\r\n";
                                    
            fw.close();
            
            String plantilla_html = obtenerPlantillaHTML();
            if(plantilla_html != null || !plantilla_html.trim().isEmpty()){
                plantilla_html = plantilla_html.replace("{{title}}", "Informe de voluntarios");
                plantilla_html = plantilla_html.replace("{{head_title}}", "Informe de voluntarios");
                plantilla_html = plantilla_html.replace("{{content}}", html);
                
                File f_html = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".html");
                FileWriter fw_html = new FileWriter(f_html);
                
                fw_html.write(plantilla_html);
                
                fw_html.close();
            }
            
            closeLoadingDialog(dlg);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generar clasificación de corredores en una carrera concreta
     * @param folder Carpeta de listados de las clasificaciones
     * @param plantilla_html Plantilla HTML
     * @param categoria Categoria de la carrera
     * @param sexo Sexo de la carrera
     * @throws IOException 
     */
    private static void generarInGanadores_Listado_Corredor(String folder, String plantilla_html, String categoria, String sexo) throws IOException{
        plantilla_html = plantilla_html.replace("{{title}}", "<a href='../ganadores.html'>Listado de ganadores</a>: Corredores de "+categoria+" ("+sexo+")");
        plantilla_html = plantilla_html.replace("{{head_title}}", "Listado de ganadores: Corredores de "+categoria+" ("+sexo+")");
        
        String html = "<table border='1'>\r\n";
        html += "\t<tr>\r\n";
        html += "\t\t<th>Posición</th>\r\n";
        html += "\t\t<th>Dorsal</th>\r\n";
        html += "\t\t<th>Nombre</th>\r\n";
        html += "\t\t<th>Apellidos</th>\r\n";
        html += "\t\t<th>Club</th>\r\n";
        html += "\t</tr>\r\n";
        for(Corredor cor : CorredorDAO.loadAll(categoria, sexo, true)){
            html += "\t<tr>\r\n";
            html += "\t\t<td>"+cor.getPuesto_llegada()+"</td>\r\n";
            html += "\t\t<td>"+cor.getDorsal()+"</td>\r\n";
            html += "\t\t<td>"+cor.getNombre()+"</td>\r\n";
            html += "\t\t<td>"+cor.getApellidos()+"</td>\r\n";
            html += "\t\t<td>"+((cor.getClub() != null) ? cor.getClub().getNombre() : "---")+"</td>\r\n";
            html += "\t</tr>\r\n";
        }
        html+="</table>\r\n";

        plantilla_html = plantilla_html.replace("{{content}}", html);

        File f_html = new File(FOLDER+"/"+folder+"/corredores_"+categoria+sexo+".html");
        FileWriter fw_html = new FileWriter(f_html);

        fw_html.write(plantilla_html);

        fw_html.close();
    }
    
    /**
     * Generar clasificación de clubes en una carrera concreta
     * @param folder Carpeta de listados de las clasificaciones
     * @param plantilla_html Plantilla HTML
     * @param categoria Categoria de la carrera
     * @param sexo Sexo de la carrera
     * @throws IOException 
     */
    private static void generarInGanadores_Listado_Club(String folder, String plantilla_html, String categoria, String sexo) throws IOException{
        plantilla_html = plantilla_html.replace("{{title}}", "<a href='../ganadores.html'>Listado de ganadores</a>: Clubs de "+categoria+" ("+sexo+")");
        plantilla_html = plantilla_html.replace("{{head_title}}", "Listado de ganadores: Clubs de "+categoria+" ("+sexo+")");
        
        String html = "<table border='1'>\r\n";
        html += "\t<tr>\r\n";
        html += "\t\t<th>Posición</th>\r\n";
        html += "\t\t<th>Nombre</th>\r\n";
        html += "\t\t<th>Pos. total corredores</th>\r\n";
        html += "\t</tr>\r\n";
        int i = 1;
        for(Club c : ClubDAO.loadAll(categoria, sexo)){
            html += "\t<tr>\r\n";
            html += "\t\t<td>"+i+"</td>\r\n";
            html += "\t\t<td>"+c.getNombre()+"</td>\r\n";
            html += "\t\t<td>"+c.getPos_total_corredores()+"</td>\r\n";
            html += "\t</tr>\r\n";
            i++;
        }
        html+="</table>\r\n";

        plantilla_html = plantilla_html.replace("{{content}}", html);

        File f_html = new File(FOLDER+"/"+folder+"/clubs_"+categoria+sexo+".html");
        FileWriter fw_html = new FileWriter(f_html);

        fw_html.write(plantilla_html);

        fw_html.close();
    }
    

    /**
     * Generar informe de ganadores
     */
    public static void generarInGanadores(){
        JDialog dlg = loadingDialog("Creando informe de ganadores...");
        
        _createFolder(FOLDER);
        
        final String NOMBRE_ARCHIVO = "ganadores";
        final String GANADORES_FOLDER = "ganadores";
        
        try{
            String plantilla_html = obtenerPlantillaHTML();
            
            if(plantilla_html != null || !plantilla_html.trim().isEmpty()){
                
                /*
                    Crear indice de carreras
                */
                plantilla_html = plantilla_html.replace("{{title}}", "Listado de ganadores: Indice");
                plantilla_html = plantilla_html.replace("{{head_title}}", "Listado de ganadores: Indice");
                
                String html = "<ul>\r\n";
                
                ArrayList<Carrera> cs = CarreraDAO.loadAll();
                for(Carrera c: cs)
                    html += "\t<li>"+c.getCategoria()+" ("+c.getSexo()+"): <a href='"+GANADORES_FOLDER+"/corredores_"+c.getCategoria()+c.getSexo()+".html'>Corredores</a> <a href='"+GANADORES_FOLDER+"/clubs_"+c.getCategoria()+c.getSexo()+".html'>Clubs</a></li>\r\n";
                
                html+="</ul>\r\n";
                
                plantilla_html = plantilla_html.replace("{{content}}", html);
                
                File f_html = new File(FOLDER+"/"+NOMBRE_ARCHIVO+".html");
                FileWriter fw_html = new FileWriter(f_html);
                
                fw_html.write(plantilla_html);
                
                fw_html.close();
                
                /*
                    Crear informes para cada carrera
                */
                _createFolder(FOLDER+"/"+GANADORES_FOLDER);

                for(Carrera c: cs){
                    generarInGanadores_Listado_Corredor(GANADORES_FOLDER, plantilla_html, c.getCategoria(), c.getSexo());
                    generarInGanadores_Listado_Club(GANADORES_FOLDER, plantilla_html, c.getCategoria(), c.getSexo());
                }
            
            }else{
                JOptionPane.showMessageDialog(null, "No se encuentra la plantilla HTML", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
            closeLoadingDialog(dlg);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}