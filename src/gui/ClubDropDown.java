/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import models.Club;

/**
 *
 * @author jaumesegarra
 */
public class ClubDropDown extends Club{
    public ClubDropDown(){
        super(0,null,null,0,null);
    }
    
    public ClubDropDown(Club c){
        super(c.getId(), c.getNombre(), c.getDireccion(), c.getTelefono(), c.getNombre_responsable());
    }
    
    public String toString(){
        return (super.getId()==0) ? "---" : super.getNombre();
    }
}
