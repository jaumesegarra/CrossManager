/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author jaumesegarra
 */
public class ButtonEditor extends DefaultCellEditor {
  protected JButton button;

  private String label;

  private boolean isPushed;
  private boolean isDeleteRow = false;
  
  private JTable table;
  private ListadoWindow lcw= null;
  private boolean isCarrera = false;
  
  public ButtonEditor(JCheckBox checkBox, ListadoWindow lcw, boolean carrera) {
      this(checkBox,lcw);
      this.isCarrera=true;
  }
  
  public ButtonEditor(JCheckBox checkBox, ListadoWindow lcw) {
    super(checkBox);
    this.lcw=lcw;
    button = new JButton();
    button.setOpaque(true);
    button.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
          fireEditingStopped();
      }
    });
  }

  public Component getTableCellEditorComponent(JTable table, Object value,
      boolean isSelected, int row, int column) {
    if (isSelected) {
      button.setForeground(table.getSelectionForeground());
      button.setBackground(table.getSelectionBackground());
    } else {
      button.setForeground(table.getForeground());
      button.setBackground(table.getBackground());
    }
    this.table= table;
    isDeleteRow = false; 
    label = (value == null) ? "" : value.toString();
    button.setText(label);
    isPushed = true;
    return button;
  }
  
  public void setDeleted(){
      isDeleteRow = true;
  }
  
  public String getLabel(){
      return label;
  }
  
  public int getSelectedRow(){
      return table.getSelectedRow();
  }
  public Object getCellEditorValue() {
      
    if (isPushed)
        lcw.ButtonTableActionEvent(this);
    
    isPushed = false;
    return new String(label);
  }

  public boolean stopCellEditing() {
    isPushed = false;
    return super.stopCellEditing();
  }

  protected void fireEditingStopped() {
    super.fireEditingStopped();
    
    if(isDeleteRow)
     {
          DefaultTableModel tableModel = (DefaultTableModel) table.getModel();                            
          tableModel.removeRow(table.getSelectedRow());   
     }
  }
}
