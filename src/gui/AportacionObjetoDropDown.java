/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import models.AportacionObjeto;

/**
 *
 * @author jaumesegarra
 */
public class AportacionObjetoDropDown extends AportacionObjeto{
    public AportacionObjetoDropDown(){
        super(0,null,null,0,0);
    }
    
    public AportacionObjetoDropDown(AportacionObjeto ap){
        super(ap.getId(), ap.getConcepto(), ap.getAportador(), ap.getCantidad(), ap.getCantidadDisponible());
    }
    
    public String toString(){
        return (super.getId()==0) ? "---" : super.getConcepto();
    }
}
