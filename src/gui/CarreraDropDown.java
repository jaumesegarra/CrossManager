/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import models.Carrera;

/**
 *
 * @author jaumesegarra
 */
public class CarreraDropDown extends Carrera{
    public CarreraDropDown(Carrera car){
        super(car.getHoraComienzo(),car.getReglas(),car.getRecorrido(),car.getCategoria(),car.getSexo());
    }
    
    public String toString(){
        return super.getCategoria() + " ("+super.getSexo()+")";
    }
}
